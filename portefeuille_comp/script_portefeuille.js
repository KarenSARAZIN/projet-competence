//variables pour l'affichage du portfolio
let url_bloc = "http://127.0.0.1:8055/items/bloc";
let url_domaine = "http://127.0.0.1:8055/items/domaine";
let url_comp = "http://127.0.0.1:8055/items/competence";
let access_token = localStorage.getItem('access_token');
let id_user = localStorage.getItem('id_user');
let url_acquis = `http://127.0.0.1:8055/items/relation_comp_user?fields=id_comp&filter[id_user][_eq]=${id_user}`;



async function affichage_portfolio() {

    let liste_comp_acquis = [];
    let liste_comp = [];

    await fetch(url_bloc, {
        method: 'GET',
        headers: {
    
            "Content-Type": 'application/json',
            "Authorization": `Bearer ${access_token}`

        },
    })
    .then((response_bloc) => response_bloc.json())
    .then((data_bloc) => {

        let fin_boucle_bloc = Object.keys(data_bloc['data']).length;

        for (let ele_bloc = 0; ele_bloc < fin_boucle_bloc; ele_bloc++) {

        
            let numero_bloc = data_bloc['data'][ele_bloc]['no_bloc'];
            let nom_bloc = data_bloc['data'][ele_bloc]['nom_bloc'];

            $("#table-comp").append(`<tr id="table-tr-bloc-${ele_bloc}">`);
            $(`#table-tr-bloc-${ele_bloc}`).append(`<td>${numero_bloc}</td> <td style="text-align:left;">${nom_bloc}</td> <td style="background-color:lightgray;"> </td>`);
            $(`#table-tr-bloc-${ele_bloc}`).append(`</tr>`);

        }
    })
    .catch((err) => {
        console.log("Erreur ->", err);
    });



    await fetch(url_domaine, {
        method: 'GET',
        headers: {
    
            "Content-Type": 'application/json',
            "Authorization": `Bearer ${access_token}`

        },
    })
    .then((response_domaine) => response_domaine.json())
    .then((data_domaine) => {

        let fin_boucle_domaine = Object.keys(data_domaine['data']).length;

        for (let ele_domaine = 0; ele_domaine < fin_boucle_domaine; ele_domaine++) {


            let numero_domaine = data_domaine['data'][ele_domaine]['no_domaine'];
            let nom_domaine = data_domaine['data'][ele_domaine]['nom_domaine'];

            $("#table-comp").append(`<tr id="table-tr-domaine-${ele_domaine}">`);
            $(`#table-tr-domaine-${ele_domaine}`).append(`<td>${numero_domaine}</td> <td style="text-align:left;">${nom_domaine}</td> <td style="background-color:lightgray;"> </td>`);
            $(`#table-tr-domaine-${ele_domaine}`).append(`</tr>`);


        }
            
    })
    .catch((err) => {
        console.log("Erreur ->", err);
    });


   
    await fetch(url_acquis, {
        method: 'GET',
        headers: {
    
            "Content-Type": 'application/json',
            "Authorization": `Bearer ${access_token}`

        },
    })
    .then((response_acquis) => response_acquis.json())
    .then((data_acquis) => {


        let fin_boucle_comp_acquis = Object.keys(data_acquis['data']).length;

        for (let ele_comp_acquis = 0; ele_comp_acquis < fin_boucle_comp_acquis; ele_comp_acquis++) {

            liste_comp_acquis.push(data_acquis['data'][ele_comp_acquis]['id_comp']);

        }

            
    })
    .catch((err) => {
        console.log("Erreur ->", err);
    }); 





    await fetch(url_comp, {
        method: 'GET',
        headers: {
    
            "Content-Type": 'application/json',
            "Authorization": `Bearer ${access_token}`

        },
    })
    .then((response_comp) => response_comp.json())
    .then((data_comp) => {

        let fin_boucle_comp = Object.keys(data_comp['data']).length;

        for (let ele_comp = 0; ele_comp < fin_boucle_comp; ele_comp++) {


            let numero_comp = data_comp['data'][ele_comp]['no_comp'];
            let nom_comp = data_comp['data'][ele_comp]['nom_comp'];

            $("#table-comp").append(`<tr id="table-tr-comp-${ele_comp}">`);
            $(`#table-tr-comp-${ele_comp}`).append(`<td>${numero_comp}</td> <td style="text-align:left;">${nom_comp}</td>`);
            $(`#table-tr-comp-${ele_comp}`).append(`</tr>`);

            liste_comp_acquis.forEach(function(id_comp_acquis) {



                let id_comp = Object.values(data_comp['data'][ele_comp])[0];


                //si l'utilisateur a validé la compétence
                if ( id_comp_acquis == id_comp ) {

                    $("#table-comp").append(`<tr id="table-tr-comp-${ele_comp}">`);
                    $(`#table-tr-comp-${ele_comp}`).append(`<td> <img src="../image/check.png" alt="validée"> </img> </td>`);
                    $(`#table-tr-comp-${ele_comp}`).append(`</tr>`);

                }


            });

        }

    }) 
    .catch((err) => {
        console.log("Erreur ->", err);
    }); 


}

affichage_portfolio();



function pdf_telechargement() {
	
    let pdf = new jsPDF('l', 'mm', 'a3');
    source = $('#contenu-envoi-pdf')[0];
    specialElementHandlers = {
        '#bypassme': function (element, renderer) {
            return true
        }
    };
    margins = {
        top: 50,
        bottom: 20,
        left: 30,
        width: 1000
    };
    pdf.fromHTML(
        source,
        margins.left,
        margins.top, {
            'width': margins.width,
            'elementHandlers': specialElementHandlers
        },

        function (dispose) {
            pdf.save('Portefeuille de compétences.pdf');
        }, margins
    );
}
