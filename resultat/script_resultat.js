let url_collab = "http://127.0.0.1:8055/users?filter[role][_eq]=40eee14b-2397-4456-919e-0c2f885178e5";
let url_resultat_crit = "http://127.0.0.1:8055/items/resultat";
let access_token = localStorage.getItem('access_token');
let id_user = "";


async function liste_collab() {

    await fetch(url_collab, {
        method: 'GET',
        headers: {
    
            "Content-Type": 'application/json',
            "Authorization": `Bearer ${access_token}`

        },
    })
    .then((response_collab) => response_collab.json())
    .then((data_collab) => {

        let fin_boucle_collab = Object.keys(data_collab['data']).length;


        for (let ele_collab = 0; ele_collab < fin_boucle_collab; ele_collab++) {

        
            let prenom_collab = data_collab['data'][ele_collab]['first_name'];
            let nom_collab = data_collab['data'][ele_collab]['last_name'];
            let id_collab = data_collab['data'][ele_collab]['id'];

            $("#collab-result").append(`<option class="selected-result" value="${id_collab}">${nom_collab} ${prenom_collab}</option>`);

        }
        
        $("#collab-result").change( async function() {

            //on enlève la div pour noter les collaborateurs
            $('#affichage-noter').remove();

            id_user = document.getElementById("collab-result").value;
            let url_resultat = `http://127.0.0.1:8055/items/relation_eval_user?filter[id_user][_eq]=${id_user}`;

        
            await fetch(url_resultat, {
                method: 'GET',
                headers: {
        
                    "Content-Type": 'application/json',
                    "Authorization": `Bearer ${access_token}`
        
                },
            })
            .then((response_note) => response_note.json())
            .then((data_note) => {
        
                $("#affichage").empty();

                //on vérifie si le collaborateur a déjà des notes
                if (data_note['data'].length == 0) {

                    $('#affichage').append(`<p id="msg-non-evalue"> Ce collaborateur n'a pas encore été évalué ! </p>`);

                } else {

                $("#affichage").append(`<table id="table-note">
                                            <tr>
                                                <td class="table-header">LIBELLE</td>
                                                <td class="table-header">CRITERE(S)</td>
                                                <td class="table-header">NOTE</td>
                                                <td class="table-header">COMMENTAIRE</td>
                                            </tr>
                                        </table>`); 
                                        
                }
 
                let fin_boucle_note = Object.keys(data_note['data']).length;
                
        
                for (let ele_note = 0; ele_note < fin_boucle_note; ele_note++) {
        
                    let libelle_note = data_note['data'][ele_note]['id_eval'];
                    let note = data_note['data'][ele_note]['note'];
                    let commentaire_eval = data_note['data'][ele_note]['commentaire'];
                    let url_eval_nom = `http://127.0.0.1:8055/items/evaluation?filter[id_eval][_eq]=${libelle_note}`;
        
                    fetch(url_eval_nom, {
                        method: 'GET',
                        headers: {
                
                            "Content-Type": 'application/json',
                            "Authorization": `Bearer ${access_token}`
                
                        },
                    })
                    .then((response_eval) => response_eval.json())
                    .then((data_eval) => {
        

                        
                        let libelle_eval = data_eval['data'][0]['nom_eval'];
                        let id_eval = data_eval['data'][0]['id_eval'];
                        
                        $("#table-note").append(`<tr class="table-tr" id="table-tr-note-${ele_note}">`);
                        $(`#table-tr-note-${ele_note}`).append(`<td id="eval-${id_eval}">${libelle_eval}</td>`);
                        $(`#table-tr-note-${ele_note}`).append(`</tr>`);
                        
        
                        fetch(url_liste_crit, {
                            method: 'GET',
                            headers: {
                
                            "Content-Type": 'application/json',
                            "Authorization": `Bearer ${access_token}`
                
                            },
                        })
                        .then((response_crit) => response_crit.json())
                        .then((data_crit) => {
                
                            let fin_boucle_crit = Object.keys(data_crit['data']).length;
   
        
                            $(`#table-tr-note-${ele_note}`).append(`<td style="text-align: left;" id="table-td-crit-${ele_note}">`);
        
                            for (let ele_crit = 0; ele_crit < fin_boucle_crit; ele_crit++) {
        
                                let libelle_crit = data_crit['data'][ele_crit]['nom_crit'];
                                let id_crit = data_crit['data'][ele_crit]['id_crit'];
                                let point = data_crit['data'][ele_crit]['point'];

                                $(`#table-td-crit-${ele_note}`).append(`<li id='li-crit-${ele_crit}' max='${point}' value='${id_crit}'> ${libelle_crit} </li>`);
        
                        
                            }

                            $(`#table-tr-note-${ele_note}`).append(`</td>`);

                            //si l'élève n'a pas de note, alors on peut le noter
                            if (note === null) {

        
                                $(`#table-tr-note-${ele_note}`).append(`<td> <button class="btn-corriger" id="${ele_note}" onclick="noter_collab(this.id);"> Noter l'évaluation </button> </td>`);
                                $(`#table-tr-note-${ele_note}`).append(`<td>Evaluation non noté</td>`);
        
                            } else {

                                let class_ajout = "";
                                //on évalue le degré d'acquisition
                                if (note < 33) {

                                    class_ajout = 'non-acquis';

                                } else if (note < 66) {


                                    class_ajout = 'partiellement-acquis';

                                } else if (note < 100) {

                                    class_ajout = 'acquis';

                                }
    
                                $(`#table-tr-note-${ele_note}`).append(`</td>`);
        
                                $(`#table-tr-note-${ele_note}`).append(`<td class="${class_ajout}">${note}%</td>`);

                                $(`#table-tr-note-${ele_note}`).append(`<td>${commentaire_eval}</td>`);
        
                            }
    
                
                        })
                        .catch((err) => {
                            console.log("Erreur ->", err);
                        });
                
                    })
                    .catch((err) => {
                        console.log("Erreur ->", err);
                    });
        
                    let url_liste_crit = `http://127.0.0.1:8055/items/critere?filter[id_eval][_eq]=${libelle_note}`;
        
    
                }
        
            })
            .catch((err) => {
                console.log("Erreur ->", err);
            });
        
        
        })
    })
    .catch((err) => {
        console.log("Erreur ->", err);
    });

}

liste_collab();


//permet de noter le collaborateur s'il n'a pas de note
function noter_collab(id_case) {

    let point_total = 0;
    //on vérifie l'id de l'évaluation
    let eval_selection = $(`#table-tr-note-${id_case}`).find('td:first').attr('id');
    eval_selection = eval_selection.split("-");
    eval_selection = eval_selection[1];

    let url_selection_eval = `http://127.0.0.1:8055/items/evaluation?filter[id_eval][_eq]=${eval_selection}`;

    //on obtient les points totaux en fonction de l'evaluation selectionné
    fetch(url_selection_eval, {
        method: 'GET',
        headers: {
    
            "Content-Type": 'application/json',
            "Authorization": `Bearer ${access_token}`
    
        },
    })
    .then((response_note) => response_note.json())
    .then((data_total) => {
    
        point_total = data_total['data'][0]['point_total'];
     
    
    }) 
    .catch((err) => {
        console.log("Erreur ->", err);
    });


    $('#affichage-noter').remove();

    $('body').append("<div id='affichage-noter'> <p id='msg-noter-crit'> Veuillez noter tous les critères: </p> </div>");

    let critere = [];
    let moyenne = 0;
    let max_point = 0;

    critere = $(`#table-td-crit-${id_case}`).text();
    critere = critere.split("  ");

    critere.forEach((element, numero_crit) => {

        max_point = $(`#table-td-crit-${id_case} #li-crit-${numero_crit}`)[0].getAttribute("max");
        
        $('#affichage-noter').append(`<label class="label-crit"> ${element} </label> <input class="noter-crit-point" id="${numero_crit}" type="number" min='0' max='${max_point}' value='0' /> <span> /${max_point} </span> <textarea class='commentaire-crit' id='${numero_crit}' placeholder="Remarque:"></textarea> <br>`);


    });

    $('#affichage-noter').append(`<hr> <textarea id='commentaire-eval' placeholder="Ajouter un commentaire sur l'évaluation ou la note:"></textarea> <br>`)

    $('#affichage-noter').append(`<button id="btn-confirmer"> Confirmer </button>`);
    
    $('#btn-confirmer').on("click", async function() {

        critere.forEach(async function(element, index) {


            let crit_note = parseInt($(`#${index}.noter-crit-point`).val(), 10);

            let id_crit = $(`#table-td-crit-${id_case} #li-crit-${index}`)[0].getAttribute("value");

            let remarque_crit = $(`#${index}.commentaire-crit`).val();

            moyenne = moyenne + crit_note;

            //on note pour chaque critère
            await fetch(url_resultat_crit, {
                method: 'POST',
                headers: {
        
                    "Content-Type": 'application/json',
                    "Authorization": `Bearer ${access_token}`
                    
                },
                body: JSON.stringify( {
        
                    'id_collab': `${id_user}`,
                    'id_crit': `${id_crit}`,
                    'id_eval': `${eval_selection}`,
                    'note_crit': `${crit_note}`,
                    'commentaire_crit': `${remarque_crit}`
        
                    }
                )
            })
            .catch((err) => {
                console.log("Erreur ->", err);
            });

        });
        //pourcentage puis arrondissement de la moyenne
        moyenne = (moyenne*100)/point_total;
        Math.round(moyenne);

        let id_note = 0;

        let url_note = `http://127.0.0.1:8055/items/relation_eval_user?filter[_and][1][id_user][_eq]=${id_user}&filter[_and][1][id_eval][_eq]=${eval_selection}`;

        await fetch(url_note, {
            method: 'GET',
            headers: {
        
                "Content-Type": 'application/json',
                "Authorization": `Bearer ${access_token}`
        
            },
        })
        .then((response_note) => response_note.json())
        .then((data_note) => {
        
            id_note = data_note['data'][0]['id_relation_eval_user'];
        
        }) 
        .catch((err) => {
            console.log("Erreur ->", err);
        });

        //après avoir récupérer id de la relation evaluation-utilisateur, on modifie la note de l'élève
        let url_note_total_commentaire = `http://127.0.0.1:8055/items/relation_eval_user/${id_note}`;

        await fetch(url_note_total_commentaire, {
            method: 'PATCH',
            headers: {
    
                "Content-Type": 'application/json',
                "Authorization": `Bearer ${access_token}`
                
            },
            body: JSON.stringify({
    
                'note': `${moyenne}`,
                'commentaire': `${document.getElementById('commentaire-eval').value}`
    
            })
        })
        .catch((err) => {
            console.log("Erreur ->", err);
        });

    
    }) 
    

}