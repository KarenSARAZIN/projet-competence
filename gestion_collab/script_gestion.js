let access_token = localStorage.getItem('access_token');
let url_collab = "http://127.0.0.1:8055/users?fields=id,last_name,first_name&filter[role][_eq]=40eee14b-2397-4456-919e-0c2f885178e5";
let url_ajout_collab = "http://127.0.0.1:8055/users";

//permet d'afficher la liste de tous les collaborateurs
function liste_collab() {

    fetch(url_collab, {
        method: 'GET',
        headers: {

        "Content-Type": 'application/json',
        "Authorization": `Bearer ${access_token}`

        },
    })
    .then((response_collab) => response_collab.json())
    .then((data_collab) => {

        
        let fin_boucle_collab = Object.keys(data_collab['data']).length;


        for (let ele_collab = 0; ele_collab < fin_boucle_collab; ele_collab++) {

        
            let prenom_collab = data_collab['data'][ele_collab]['first_name'];
            let nom_collab = data_collab['data'][ele_collab]['last_name'];
            let id_collab = data_collab['data'][ele_collab]['id'];

            $("#table-gestion").append(`<tr id='${id_collab}'> <td> ${nom_collab} </td> <td> ${prenom_collab} </td> <td id='${ele_collab}'> <select class='select-gestion' onchange='modification(this.value, $(this).parent()[0].id);'> <option>...</option> <option value="supprimer"> supprimer </option> </select> </td> </tr>`);

        }


    })
    .catch((err) => {
        console.log("Erreur ->", err);
    });

}


//on affiche le formulaire d'ajout
function affichage_formulaire() {

    $('#formulaire-ajout').remove();

    $('body').append(`<div id="div-formulaire-ajout">

                        <img id="btn-fermer" src="../image/plus-button.png" onclick="$('#div-formulaire-ajout, #img-check, #msg-valid').remove();" />


                        <h2 id="h2-formulaire-ajout"> Formulaire d'ajout: </h2>

                        <input type="text" id='nom_famille' placeholder="Nom de famille" required>
                        <br>
                        <input type="text" id='prenom' placeholder="Prénom" required>
                        <br>
                        <input type="mail" id='email' placeholder="E-mail" required>
                        <br>
                        <input type="password" id='mdp' placeholder="Mot de passe" required>
                        <br>
                        <button id="btn-confirmer-ajout"> Confirmer l'ajout </button>


                    </div>`);

    //permet de confirmer l'ajout
    $('#btn-confirmer-ajout').on('click', function() {
        

        $('#img-check, #msg-valid').remove();

        //on reprend toutes les valeurs du formulaire
        let prenom = document.getElementById('prenom').value;
        let nom_famille = document.getElementById('nom_famille').value;
        let email = document.getElementById('email').value;
        let mdp = document.getElementById('mdp').value;


        fetch(url_ajout_collab, {
            method: 'POST',
            headers: {
    
                "Content-Type": 'application/json',
                "Authorization": `Bearer ${access_token}`
                
            },
            body: JSON.stringify( {
    
                'first_name': `${prenom}`,
                'last_name': `${nom_famille}`,
                'email': `${email}`,
                'password': `${mdp}`,
                'role': `40eee14b-2397-4456-919e-0c2f885178e5`
    
            })
        })
        .catch((err) => {
            console.log("Erreur ->", err);
        });
        

    }) 

}

//on vérifie quel option a été sélectionné
function modification(valeur, id) {

    let id_collab = $(`#${id}`).parent()[0].id;
    let url_suppr = `http://127.0.0.1:8055/users/${id_collab}`;


    //on vérifie le type de modification
    if (valeur == 'supprimer') {

        $('#img-check, #msg-valid').remove();

        if (confirm("Êtes-vous sûr de vouloir supprimer ce collaborateur ? Cette action est irréversible.")) {


            fetch(url_suppr, {
                method: 'DELETE',
                headers: {
    
                "Content-Type": 'application/json',
                "Authorization": `Bearer ${access_token}`
    
                },
            })
            .catch((err) => {
                console.log("Erreur ->", err);
            }); 

            $('body').append(`<img id="img-check" src="../image/check.png" alt="validée"> <span id="msg-valid"> L'utilisateur a bien été supprimé ! </span> </img>`);

        }

    }

}



liste_collab();