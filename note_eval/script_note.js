let access_token = localStorage.getItem('access_token');
let id_user = localStorage.getItem('id_user');
let liste_note = [];
let moyenne = 0;
let url_note = `http://127.0.0.1:8055/items/relation_eval_user?filter[id_user][_eq]=${id_user}`;



async function affichage_evaluation() {

    await fetch(url_note, {
        method: 'GET',
        headers: {

            "Content-Type": 'application/json',
            "Authorization": `Bearer ${access_token}`

        },
    })
    .then((response_note) => response_note.json())
    .then((data_note) => {


        //on vérifie si le collaborateur a déjà des notes
        if (data_note['data'].length == 0) {

            $('body').append(`<p id="msg-non-evalue"> Vous n'avez aucune évaluation en cours ou terminé ! </p>`);

        } else {


            $('#div-h2').append(`<h2>Notes des évaluations</h2>`);

            $('#affichage').append(`<table id="table-note">
                                        <tr>
                                            <td class="table-header">LIBELLE</td>
                                            <td class="table-header">CRITERE(S)</td>
                                            <td class="table-header">COMMENTAIRE</td>
                                            <td class="table-header">NOTE</td>
                                        </tr>
                                    </table>`);

            let fin_boucle_note = Object.keys(data_note['data']).length;

            for (let ele_note = 0; ele_note < fin_boucle_note; ele_note++) {

                let libelle_note = data_note['data'][ele_note]['id_eval'];
                let note = data_note['data'][ele_note]['note'];
                let commentaire_eval = data_note['data'][ele_note]['commentaire'];
                let url_eval_nom = `http://127.0.0.1:8055/items/evaluation?filter[id_eval][_eq]=${libelle_note}`;
    
                //on additionne les notes pour faire la moyenne plus tard (en évitant les N.Notés)
                if (note !== null) {
    
                    liste_note.push(note);
    
                }
    
                fetch(url_eval_nom, {
                    method: 'GET',
                    headers: {
            
                        "Content-Type": 'application/json',
                        "Authorization": `Bearer ${access_token}`
            
                    },
                })
                .then((response_eval) => response_eval.json())
                .then((data_eval) => {
                    
                    let libelle_eval = data_eval['data'][0]['nom_eval'];
                    
                    $("#table-note").append(`<tr id="table-tr-note-${ele_note}">`);
                    $(`#table-tr-note-${ele_note}`).append(`<td>${libelle_eval}</td>`);
                    $(`#table-tr-note-${ele_note}`).append(`</tr>`);
                    
                    
                    fetch(url_liste_crit, {
                        method: 'GET',
                        headers: {
            
                        "Content-Type": 'application/json',
                        "Authorization": `Bearer ${access_token}`
            
                        },
                    })
                    .then((response_crit) => response_crit.json())
                    .then((data_crit) => {
            
                        let fin_boucle_crit = Object.keys(data_crit['data']).length;
    
    
                        $(`#table-tr-note-${ele_note}`).append(`<td style="text-align: left;" id="table-td-crit-${ele_note}">`);

    
                        for (let ele_crit = 0; ele_crit < fin_boucle_crit; ele_crit++) {
    
                            let libelle_crit = data_crit['data'][ele_crit]['nom_crit'];
                            $(`#table-td-crit-${ele_note}`).append(`<li id="li-crit-${ele_crit}"> ${libelle_crit}: </li>`);
    
                    
                        }

                        $(`#table-tr-note-${ele_note}`).append(`</td>`);

                        fetch(url_crit_note, {
                        method: 'GET',
                        headers: {
            
                        "Content-Type": 'application/json',
                        "Authorization": `Bearer ${access_token}`
            
                        },
                    })
                    .then((response_crit_note) => response_crit_note.json())
                    .then((data_crit_note) => {
                        
                        data_crit_note['data'].forEach((element, index) => {

                            console.log(element)
                            let note_crit = element['note_crit'];
                            let commentaire_crit = element['commentaire_crit'];
                            $(`#table-td-crit-${ele_note} #li-crit-${index}`).append(`<span title="${commentaire_crit}"> ${note_crit} </span>`);
                            
                        })
    
                    })
                    .catch((err) => {
                        console.log("Erreur ->", err);
                    });
                        //si l'utilisateur n'a pas de note, on affichera non noté
                        if (note === null) {
    
    
                            $(`#table-tr-note-${ele_note}`).append(`<td> Pas de commentaire disponible </td>`);
                            $(`#table-tr-note-${ele_note}`).append(`<td> N.Noté </td>`);
    
                        } else {


                            let class_ajout = "";
                            //on évalue le degré d'acquisition
                            if (note < 33) {

                                class_ajout = 'non-acquis';

                            } else if (note < 66) {


                                class_ajout = 'partiellement-acquis';

                            } else if (note <= 100) {

                                class_ajout = 'acquis';

                            }

                            $(`#table-tr-note-${ele_note}`).append(`<td>${commentaire_eval}</td>`);
                            $(`#table-tr-note-${ele_note}`).append(`<td class="${class_ajout}">${note}%</td>`);
    
                        }
    
                    })
                    .catch((err) => {
                        console.log("Erreur ->", err);
                    });

            
                })
                .catch((err) => {
                    console.log("Erreur ->", err);
                });

    
                let url_liste_crit = `http://127.0.0.1:8055/items/critere?filter[id_eval][_eq]=${libelle_note}`;

                let url_crit_note = `http://127.0.0.1:8055/items/resultat?filter[_and][1][id_collab][_eq]=${id_user}&filter[_and][1][id_eval][_eq]=${libelle_note}`;
    
            }

            //on affiche la moyenne
            liste_note.forEach(function(note) {


                if (note != 0) {

                    moyenne += note;

                }

            });

            //on évite de diviser par 0, s'il n'y a pas d'évaluation
            if (liste_note.length == 0) {

                moyenne = 0;

            } else {

                moyenne /= liste_note.length;

            }

            $("#table-note").append(`<tr id="table-tr-moyenne"> <td id="moyenne-note" colspan="3"> Moyenne des notes: </td> <td id="table-td-moyenne">${moyenne}%</td> </tr>`);


        }


    })
    .catch((err) => {
        console.log("Erreur ->", err);
    })

    
}

affichage_evaluation();
            