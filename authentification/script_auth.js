async function connexion() {

    //initialisation des variables
    let email = document.getElementById("email").value;
    let mdp = document.getElementById("mdp").value;
    let url_auth = "http://127.0.0.1:8055/auth/login";
    let url_role = "http://127.0.0.1:8055/users/me";


    //grâce au mdp et à l'email, envoi d'une requête pour récupérer le token
    await fetch(url_auth, {
        method: 'POST',
        headers: {

            "Content-Type": 'application/json'

        },
        body: JSON.stringify( {
                'email': `${email}`,
                'password': `${mdp}`
            }
        )
    })
    .then((response) => response.json())
    .then((data) => {

        let access_token = data['data']['access_token'];
        localStorage.setItem('access_token', access_token);
        let refresh_token = data['data']['refresh_token'];
        localStorage.setItem('refresh_token', refresh_token);

        //vérifie le rôle de l'utilisateur, est-il un collaborateur ou un RH ?
        fetch(url_role, {
            method: 'GET',
            headers: {
    
                "Content-Type": 'application/json',
                "Authorization": `Bearer ${localStorage.getItem('access_token')}`
            
            },
        })
        .then((response) => response.json())
        .then((data) => {

            let admin_access = data['data']['role'];
            localStorage.setItem("id_user", data['data']['id']);

            if ( admin_access == "6db62a8f-8ee8-49fb-b4e0-b947d786f80f" ) {
    
                window.location.href = "../espace_administrateur/espace_admin.html";
    
            } else {
    
                window.location.href = "../espace_collaborateur/espace_collab.html";
            
            }
        })
        .catch((err) => {
            console.log("Erreur ->", err);
        });


    })
    .catch((err) => {
        $("#auth h2").remove();
        $("#auth").append("<h2 style='text-align:center;'>Erreur d'identifiant ou de mot de passe. Veuillez réessayer. </h2>");
        console.log("Erreur ->", err);
    });

}