//récupération de la liste des utilisateurs séléctionnable pour l'évaluation

function liste_collab(no_collab) {

    let url_collab = "http://127.0.0.1:8055/users?fields=first_name,last_name,id&filter[role][_eq]=40eee14b-2397-4456-919e-0c2f885178e5";
    let access_token = localStorage.getItem('access_token');


    fetch(url_collab, {
        method: 'GET',
        headers: {
    
            "Content-Type": 'application/json',
            "Authorization": `Bearer ${access_token}`

        },
    })
    .then((response_collab) => response_collab.json())
    .then((data_collab) => {

        let fin_boucle_collab = Object.keys(data_collab['data']).length;

        for (let ele_collab = 0; ele_collab < fin_boucle_collab; ele_collab++) {

        
            let prenom_collab = data_collab['data'][ele_collab]['first_name'];
            let nom_collab = data_collab['data'][ele_collab]['last_name'];
            let id_collab = data_collab['data'][ele_collab]['id'];

            $(`#collab-${no_collab}`).append(`<option value="${id_collab}">${nom_collab} ${prenom_collab}</option>`);
                
        
        localStorage.setItem('no_collab', no_collab);

        }
    })
    .catch((err) => {

        console.log("Erreur ->", err);

    });
}

liste_collab(1);

let no_crit = 1;
localStorage.setItem('no_crit', no_crit);

//ajout d'une nouvelle zone de texte lorsqu'on appuie sur le bouton
function ajout_crit() {

    let no_crit = Number.parseInt(localStorage.getItem("no_crit"))+1;
    localStorage.setItem('no_crit', no_crit);
    $("#div-critere").append(`<br><input id='crit-${no_crit}' class='crit-input' type='text' placeholder='-- Nommer votre critère --'>
                            <input id='crit-point-${no_crit}' class='crit-point-input' type='number' min='0' value='0'>`);

}


//ajout d'une nouvelle sélection de collaborateur lorsqu'on appuie sur le bouton
function ajout_collab() {

    let no_collab = Number.parseInt(localStorage.getItem("no_collab"))+1;
    liste_collab(no_collab);
    $("#div-collab").append(`<br><select class="collab-liste" id='collab-${no_collab}'><option value=''>-- Sélectionner un utilisateur --</option> </select>`);
    
}


//envoi à Directus des informations pour créer l'évaluation
async function confirme_eval() {

    //récupère l'id de l'eval en cours de création pour les critères
    let url_get_eval_id;
    let id_eval;
    let point_total = 0;
    let point_crit = 0;
    let url_ajout_point_total;

    let nom_eval = document.getElementById("nom-eval").value;
    let access_token = localStorage.getItem('access_token');
    let url_ajout_eval = `http://127.0.0.1:8055/items/evaluation`;
    let url_ajout_crit = `http://127.0.0.1:8055/items/critere`;
    let url_ajout_collab = `http://127.0.0.1:8055/items/relation_eval_user`;
    

    await fetch(url_ajout_eval, {
        method: 'POST',
        headers: {

            "Content-Type": 'application/json',
            "Authorization": `Bearer ${access_token}`
            
        },
        body: JSON.stringify( {

            'nom_eval': `${nom_eval}`

            }
        )
    })
    .then(

        url_get_eval_id = `http://127.0.0.1:8055/items/evaluation?filter[nom_eval][_eq]=${nom_eval}`

    )
    .catch((err) => {
        console.log("Erreur ->", err);
    });

    let nb_collab = $(".collab-liste").length;
    let nb_crit = $(".crit-input").length;


    await fetch(url_get_eval_id, {
        method: 'GET',
        headers: {

            "Content-Type": 'application/json',
            "Authorization": `Bearer ${access_token}`
            
        },
    })
    .then((response) => response.json())
    .then((res) => {

        id_eval = res['data'][0]['id_eval'];

    })
    .catch((err) => {

        console.log("Erreur ->", err);

    });
    
    for (i=1; i<nb_collab+1; i++) {

        let id_collab = $(`select#collab-${i}`).val();
        console.log(id_collab);

        fetch(url_ajout_collab, {
            method: 'POST',
            headers: {
    
                "Content-Type": 'application/json',
                "Authorization": `Bearer ${access_token}`
                
            },
            body: JSON.stringify( {
    
                'id_user': `${id_collab}`,
                'id_eval': `${id_eval}`
    
                }
            )
        })
        .catch((err) => {
            console.log("Erreur ->", err);
        });
        

    }


    for (j=1; j<nb_crit+1; j++) {

        let nom_crit = $(`#crit-${j}`).val();
        point_crit = parseInt($(`#crit-point-${j}`).val(), 10);
        point_total = point_total + point_crit;

        fetch(url_ajout_crit, {
            method: 'POST',
            headers: {
    
                "Content-Type": 'application/json',
                "Authorization": `Bearer ${access_token}`
                
            },
            body: JSON.stringify( {
    
                'nom_crit': `${nom_crit}`,
                'id_eval': `${id_eval}`,
                'point': `${point_crit}`
    
                }
            )
        })
        .catch((err) => {
            console.log("Erreur ->", err);
        });

    }

    url_ajout_point_total = `http://127.0.0.1:8055/items/evaluation/${id_eval}`;

    fetch(url_ajout_point_total, {
        method: 'PATCH',
        headers: {

            "Content-Type": 'application/json',
            "Authorization": `Bearer ${access_token}`
            
        },
        body: JSON.stringify( {

            'point_total': `${point_total}`

            }
        )
    })
    .catch((err) => {
        console.log("Erreur ->", err);
    });

    $('#img-check, #msg-valid').remove();
    
    $('body').append(`<img id="img-check" src="../image/check.png" alt="validée"> <span id="msg-valid"> L'évaluation a été correctement ajoutée ! </span>  </img>`);

}

