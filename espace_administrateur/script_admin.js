function deconnexion() {
    let tokenR = localStorage.getItem('refresh_token');
    let url = "http://127.0.0.1:8055/auth/logout";

    fetch(url, {
        method: 'POST',
        headers: {

            "Content-Type": 'application/json'

        },
        body: JSON.stringify( {
                'refresh_token': `${tokenR}`
            }
        )
    })
    .then((response) => response.json())
    .then(
        window.location.href = "../authentification/auth.html",
        localStorage.clear()
    )
    .catch((err) => {
        console.log("Erreur ->", err);
    });
}